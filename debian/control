Source: devpi-common
Maintainer: Debian Python Modules Team <python-modules-team@lists.alioth.debian.org>
Uploaders: Nicolas Dandrimont <olasd@debian.org>
Section: python
Priority: optional
Build-Depends: debhelper-compat (= 11),
               dh-python (>= 2~),
               mercurial,
               python3-all,
               python3-pip,
               python3-pkg-resources,
               python3-pytest,
               python3-requests,
               python3-setuptools,
               python3-wheel
Standards-Version: 4.1.4
Homepage: https://www.devpi.net/
Vcs-Git: https://salsa.debian.org/python-team/modules/devpi-common.git
Vcs-Browser: https://salsa.debian.org/python-team/modules/devpi-common

Package: python3-devpi-common
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}
Description: PyPI server and packaging/testing/release tool - Common modules
 devpi provides a powerful PyPI-compatible server and complementary
 command-line tool to drive packaging, testing and release activities with
 Python.
 .
 Its main features are:
  - fast PyPI mirror
  - uploading, testing and staging with private indexes
  - index inheritance
  - web interface and search
  - replication
  - import/export
  - Jenkins integration
 .
 This package provides the base modules common to both devpi's server
 and client components.
